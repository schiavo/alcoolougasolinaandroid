package com.example.vitorschiavo.alcoolougasolina;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public TextView resultado, alcool, gasolina;
    public Button calculaCombustivel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultado           = (TextView)findViewById(R.id.resultadoLegenda);
        alcool              = (TextView)findViewById(R.id.precoAlcoolCampo);
        gasolina            = (TextView)findViewById(R.id.precoGasolinaCampo);
        calculaCombustivel =  (Button) findViewById(R.id.calcular);

        calculaCombustivel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //validando valores digitados.
                Boolean validaCampos = validarCampos(alcool, gasolina);

                if (validaCampos) {
                    //calcular melhor combutivel
                    calcularMelhorPreco(alcool, gasolina);
                }else {
                    resultado.setText("Digite os preços para calcular !");
                }

            }
        });


    }

    public void calcularMelhorPreco(TextView alcool, TextView gasolina) {

        Double resultadoPreco = 0.00;
        String melhorAlcool = "melhor utilizar alcool";
        String melhorGasolina = "melhor utilizar gasolina";

        String stringOfDoubleValue = alcool.getText().toString();
        double doubleFromString = Double.parseDouble(stringOfDoubleValue);

        String stringOfDoubleValue2 = gasolina.getText().toString();
        double doubleFromString2 = Double.parseDouble(stringOfDoubleValue2);

        Boolean validaCampos = validarCampos(alcool, gasolina);


        if (validaCampos) {

            resultadoPreco = doubleFromString/doubleFromString2;

                if (resultadoPreco >= 0.7) {
                    resultado.setText(melhorGasolina);
                } else {
                    resultado.setText(melhorAlcool);
                }

        }
    }

    public Boolean validarCampos(TextView alcool, TextView gasolina) {

        Boolean camposValidados = true;

        if(alcool.getText().toString().isEmpty()){
            camposValidados = false;
        }else if(gasolina.getText().toString().isEmpty()){
            camposValidados = false;
        }

        return camposValidados;
    }


}
